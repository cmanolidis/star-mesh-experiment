######################## ADHOC ########################
#apt-get -y install git dnsmasq hostapd bridge-utils


APnet=255.255.255.0

#Wi-Fi AP and ADHOC interface name.
ifADHOC=wlan0
ifAP=ap0
#essid name and channel for B.A.T.M.A.N. ad-hoc  nodes.
ESSID=adhoc-test
CHANNEL=11
#Cell ID of ad-hoc nodes.
CELLID=02:12:34:56:78:9A

declare -A AccesPointIPs

#########################################################
###### Configuration of the topology that we using ######
#########################################################

#Define the IPs for each node's Access Point subnet.
AccesPointIPs["node20-6"]=192.168.1.2
AccesPointIPs["node14-1"]=192.168.1.3
AccesPointIPs["node7-1"]=192.168.2.2
#AccesPointIPs["node14-7"]=192.168.1.1
#AccesPointIPs["node7-7"]=192.168.2.1
AccesPointIPs["node1-6"]=192.168.2.3
AccesPointIPs["node20-14"]=192.168.3.2
#AccesPointIPs["node13-13"]=192.168.3.1
#AccesPointIPs["node7-14"]=192.168.4.1
AccesPointIPs["node1-14"]=192.168.4.2
AccesPointIPs["node14-20"]=192.168.3.3
AccesPointIPs["node6-20"]=192.168.4.3

#echo ${AccesPointIPs[*]}
#########################################################

#Find out which node is this.
HOST=$(hostname -s)
apIP=$(echo ${AccesPointIPs["$HOST"]})

#Choose the appropriate wireless card for this node.
modprobe ath9k
ARG=$(echo $(ifconfig $ifADHOC) | awk '{print$1;}')
if ! [ "$ARG" == $ifADHOC ]; then
    echo "ath9k does not supported we use: ath5k"
    modprobe ath5k
fi

ifconfig $ifADHOC up
ifconfig $ifADHOC down
ifconfig $ifADHOC mtu 1532
iwconfig $ifADHOC mode ad-hoc essid $ESSID ap $CELLID channel $CHANNEL
modprobe batman-adv debug=2
batctl if add $ifADHOC
ifconfig $ifADHOC up
sysctl -w net.ipv4.ip_forward=1

ip link set up dev bat0
ip addr replace dev bat0 $apIP/24

#batctl bl 1
#batctl ap 1

######################## ROUTING ########################
#ifconfig bat0 $apIP netmask $APnet

SUBNET=$(echo ${apIP} | cut -d"." -f1-3)
route del -net $SUBNET".0" netmask 255.255.255.0 bat0
route add -net 192.168.0.0 netmask 255.255.0.0 bat0


echo "$NODE is ready."

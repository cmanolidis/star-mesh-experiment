HOSTS="root@node14-7 root@node7-7 root@node13-13 root@node7-14"

#run scripts to hosts
for host in $HOSTS; do
    scp -o "StrictHostKeyChecking no" "adhoc-bat.sh" ${host}:/root/
    ssh -o "StrictHostKeyChecking no" ${host} "bash adhoc-bat.sh"
done

echo ""
echo ""
echo "Node: $(hostname -s)"

for i in {1..4}
do
    for j in {1..3}
    do
        IPaddr=192.168."$i"."$j"
        if ! [[ $(echo $(ifconfig bat0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-4) == $IPaddr ]]; then
            echo "Node: $(hostname -s), Ping IP address: $IPaddr"
            batctl ping $IPaddr -c 1
            echo ""
        fi
    done
done

netIF=bat0
HOST=$(hostname -s)

if [[ $HOST == "node14-7" ]] || [[ $HOST == "node7-7" ]] || [[ $HOST == "node13-13" ]] || [[ $HOST == "node7-14" ]]; then
    netIF=br0
fi

echo "Node: $(hostname -s)"
for i in {1..4}
do
    for j in {1..3}
    do
        IPaddr=192.168."$i"."$j"
        if ! [[ $(echo $(ifconfig bat0 | grep -Eo 'inet (addr:)?([0-9]*\.){3}[0-9]*' | grep -Eo '([0-9]*\.){3}[0-9]*') | cut -d"." -f1-4) == $IPaddr ]]; then
            #MACaddr=$(batctl translate $IPaddr)
            echo "Scan address: $IPaddr"
            arp-scan -I $netIF $IPaddr
            echo ""
        fi
    done
done

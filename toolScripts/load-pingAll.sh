NODES="root@node14-7 root@node20-6 root@node14-1 root@node7-7 root@node7-1 root@node1-6 root@node13-13 root@node20-14 root@node14-20 root@node7-14 root@node6-20 root@node1-14"

#run scripts to hosts
for node in $NODES; do
    scp -o "StrictHostKeyChecking no" "Scripts/pingAll.sh" ${node}:/root/
done

CLIENTS="root@node14-1 root@node7-1 root@node1-14 root@node20-14 root@node20-6 root@node1-6 root@node6-20 root@node14-20"

#load and run scripts on each client
for client in $CLIENTS; do
    scp -o "StrictHostKeyChecking no" "adhoc-client.sh" ${client}:/root/
    ssh -o "StrictHostKeyChecking no" ${client} "bash adhoc-client.sh"
done
